package com.xpx.training.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionDemo {

    public static void main(String[] args) {
    	
            String name = "Sam";           

            boolean wasExceptionThrown = false;

            System.out.println("Let's play a game with exceptions.");
            System.out.println();
            System.out.println("I'm going to give you a name and you provide me an index of a character to return from that String.");
            System.out.println("The goal of this game is to NOT throw an exception, but go ahead and try and break it if you want. (I won't tell anyone)");
            System.out.println();
            System.out.println("We're going to use the String: \"" + name + "\"");
            System.out.println();
            System.out.println("Alright, let's start.");
            System.out.println();
            System.out.print("Give me an index value: ");

            try {
            	Scanner userInput = new Scanner(System.in);
            	
                int charIndex = userInput.nextInt();
                
                userInput.close();
                
                char charFromName = name.charAt(charIndex);
                
                System.out.println("The char at index [" + charIndex + "] is '" + charFromName + "'");
                                              
            } catch (NullPointerException e) {

                System.out.println("Sorry, that doesn't exist dude.");
                System.out.println("You threw a(n) " + e.toString());
                
                if (e.getMessage() != null) {
                	System.out.println("You can't do that because: \n" + e.getMessage());
                }
                
                System.out.println();

                wasExceptionThrown = true;
                    
            } catch (InputMismatchException e) {

                System.out.println("You gave me something weird man, I can't use that.");
                System.out.println("You threw a(n) " + e.toString());
                
                if (e.getMessage() != null) {
                	System.out.println("You can't do that because: \n" + e.getMessage());
                }
                
                System.out.println();

                wasExceptionThrown = true;

            } catch (IndexOutOfBoundsException e) {

                System.out.println("You can't go that far kid.");
                System.out.println("You threw a(n) " + e.toString());
                
                if (e.getMessage() != null) {
                	System.out.println("You can't do that because: \n" + e.getMessage());
                }
                
                System.out.println();
                
                wasExceptionThrown = true;

            } catch (Exception e) {                          

                System.out.println("I'm not sure what you did, but you can't do that.");
                System.out.println("You threw a(n) " + e.toString());
                
                if (e.getMessage() != null) {
                	System.out.println("You can't do that because: \n" + e.getMessage());
                }
                
                System.out.println();

                wasExceptionThrown = true;

            } finally {

            	System.out.println("You made it to the end of the game. Let's see, how well did you do?");
            	
            	if (wasExceptionThrown == true) {
            		
            		System.out.println("You messed up and threw an exception... Good work! I guess...");
            		
            	} else {
            		
            		System.out.println("You didn't throw any exceptions. Well done!");
            		
            	}            	
            }
    }
   

    public static void exceptionMethod(String name) {

           

           

    }
    
}